service mysql start

# Config Access
chown -R www-data /var/www/*
chmod -R 755 /var/www/*

# SSL
mkdir	/etc/nginx/ssl
openssl req -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /etc/nginx/ssl/fts.pem -keyout /etc/nginx/ssl/fts.key -subj "/C=RU/ST=Moscow/L=Moscow/O=21 School/OU=gmegga/CN=fts"

# Config NGINX
mv -f ./tmp/fts /etc/nginx/sites-available/default
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/fts

# Config MYSQL
echo "CREATE DATABASE wordpress;" | mysql -u root --skip-password
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'root'@'localhost' WITH GRANT OPTION;" | mysql -u root --skip-password
echo "update mysql.user set plugin='mysql_native_password' where user='root';" | mysql -u root --skip-password
echo "FLUSH PRIVILEGES;" | mysql -u root --skip-password

# Config phpmyadmin
mkdir /var/www/html/phpmyadmin
wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.tar.gz
tar -xvf phpMyAdmin-4.9.0.1-all-languages.tar.gz --strip-components 1 -C /var/www/html/phpmyadmin
mv ./tmp/phpmyadmin.inc.php /var/www/html/phpmyadmin/config.inc.php

# Config wordpress
cd /tmp/
wget -c https://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz
mv wordpress/ /var/www/html
mv /tmp/wp-config.php /var/www/html/wordpress

rm -rf /etc/nginx/sites-enabled/default

# Trash remove
rm -rf /var/www/html/index.nginx-debian.html
rm -rf /phpMyAdmin-4.9.0.1-all-languages.tar.gz
rm -rf /tmp/*

service php7.3-fpm start
service nginx start
bash
