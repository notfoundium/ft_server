#Docker OS

FROM debian:buster

#Pre-installation

RUN apt-get update
RUN apt-get install -y procps
RUN apt-get install -y vim
RUN apt-get install -y wget

#NGINX & services installation

RUN apt-get -y install php7.3-fpm php7.3-common php7.3-mysql php7.3-gmp php7.3-curl php7.3-intl php7.3-mbstring php7.3-xmlrpc php7.3-gd php7.3-xml php7.3-cli php7.3-zip php7.3-soap php7.3-imap
RUN apt-get -y install nginx
RUN apt-get -y install mariadb-server

#Server configuration

COPY ./srcs/init.sh ./
COPY ./srcs/fts ./tmp/fts
COPY ./srcs/phpmyadmin.inc.php ./tmp/phpmyadmin.inc.php
COPY ./srcs/wp-config.php ./tmp/wp-config.php

#Initialisation

CMD bash init.sh
EXPOSE 80 443
